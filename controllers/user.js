const userModel = require('../models/user');

module.exports = class User {

  static getAll(){
    return userModel.getAllUsers();
  }

  static getOneById(req){
    return userModel.getUserById(req.params.id);
  }

  static addUser(req){
    return userModel.addUser(req.body);
  }

  static async updateUser(req){
    const { n } = await userModel.updateUser(req.params.id, req.body);
    return { success: !!n };
  }

  static async deleteUser(req){
    const { n } = await userModel.deleteUser(req.params.id);
    return { success: !!n };
  }
}