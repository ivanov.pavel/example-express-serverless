const { safeExec } =  require('../utils');
const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');

router.get('', safeExec(userController.getAll));
router.post('', safeExec(userController.addUser));
router.get('/:id', safeExec(userController.getOneById));
router.delete('/:id', safeExec(userController.deleteUser));
router.put('/:id', safeExec(userController.updateUser));

module.exports = router;