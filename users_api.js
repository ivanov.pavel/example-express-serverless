const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mongo_url = 'mongodb://example:example123@ds141328.mlab.com:41328/users';
const userRouter = require('./routes/user');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect(mongo_url, { useNewUrlParser: true });

app.use('/users', userRouter);

module.exports.handler = serverless(app);