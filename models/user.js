const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String,
    unique: true
  },
});

const model = mongoose.model('User', userSchema);

model.getAllUsers = function() {
  return this.find().select('email name');
};

model.getUserById = function(id){
  return this.findById(id).select('email name');
};

model.addUser = function(user){
  return this.create(user);
};

model.updateUser = function(_id, fields){
  return this.updateOne({ _id }, fields);
};

model.deleteUser = function(_id){
  return this.deleteOne({ _id });
};



module.exports = model;