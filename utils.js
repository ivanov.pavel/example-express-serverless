module.exports.safeExec = function safeExec(handler){
  return async function(req, res) {
    try {
        const result = await handler(req);
        res.json(result);
      } catch (e) {
        res.status(e.status || 500).json({ error: e.message });
      }
  }
};

