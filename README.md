#Small example Express + Serverless with AWS lambda


For testing deploy, you need run command deploy in package.json.
Before it you need install node_modules and setup your AWS credentials on environment
````
export AWS_ACCESS_KEY_ID=<your-key-here>
export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
````
Also you can see how it work on URL: 
https://oblmtyvln0.execute-api.us-east-1.amazonaws.com/dev/users


**USERS**

|Route|Method|Request Body |
| :---|:---:|:---|
| /| GET | --
| /| POST | {"email": "any email", "name": "any name"}
| /:id| GET | --
| /:id| PUT | {"email": "any email", "name": "any name"}
| /:id| DELETE | --